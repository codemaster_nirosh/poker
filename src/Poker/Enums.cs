﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    enum Winner
    {
        One,
        Two,
        None
    }


    enum Kind
    {
        D,
        C,
        S,
        H
    }

    enum Value
    {
        T = 10,
        J = 11,
        Q = 12,
        K = 13,
        A = 14
    }

    enum Rank
    {
        HighCard = 1,
        Pair = 2,
        TwoPairs = 3,
        ThreeOfAKind = 4,
        Straight = 5,
        Flush = 6,
        FullHouse = 7,
        FourOfKind = 8,
        StraightFlush = 9,
        RoyalFlush = 10
    }
}
