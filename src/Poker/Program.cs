﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {


                if (args.Count() == 0)
                    throw new ApplicationException("No data file path found! \n\r   Usage : poker.exe <data file path>");
                string filePath = args[0];


                int playerOneWins = 0;
                int playerTwoWins = 0;

                string[] lines = File.ReadAllLines(filePath);

                foreach (var line in lines)
                {
                    var splittedCards = line.Split(new[] { ' ' }).ToList();
                    Player playerOneHand = new Player(splittedCards.Take(5).ToList());
                    Player playerTwoHand = new Player(splittedCards.Skip(5).Take(5).ToList());
#if DEBUG
                Console.WriteLine("player 1: " + playerOneHand.ToString());
                Console.WriteLine("player 2: " + playerTwoHand.ToString());
#endif
                    var winner = Evaluate(playerOneHand, playerTwoHand);
                    if (winner == Winner.One)
                        playerOneWins++;
                    else if (winner == Winner.Two)
                        playerTwoWins++;

                    //Console.WriteLine("win : " + winner.ToString());
                }



                Console.WriteLine("Player 1: " + playerOneWins);
                Console.WriteLine("Player 2: " + playerTwoWins);
            }
            catch (Exception ex)
            {

                Console.WriteLine("Error:" + ex.Message);
               
                if (ex.InnerException != null)
                {
                    Console.WriteLine(ex.InnerException);
                }
            }


            Console.Read();
        }

        private static Winner Evaluate(Player playerOneHand, Player playerTwoHand)
        {
            Rank playerOneRank = playerOneHand.CalculateRank();
            Rank playerTwoRank = playerTwoHand.CalculateRank();


#if DEBUG

            global::System.Console.WriteLine("Player One Rank : " + playerOneRank);
            global::System.Console.WriteLine("Player Two Rank : " + playerTwoRank);
#endif

            if (playerOneRank > playerTwoRank)
                return Winner.One;
            else if (playerOneRank < playerTwoRank)
                return Winner.Two;
            else
            {

                //compare rank madeup cards 
                playerOneHand.RankMadeUpCards.Sort(new CardValueComparer());
                playerTwoHand.RankMadeUpCards.Sort(new CardValueComparer());

                for (int j = 0; j < playerOneHand.RankMadeUpCards.Count; j++)
                {
                    if (playerOneHand.RankMadeUpCards[j].TrueValue > playerTwoHand.RankMadeUpCards[j].TrueValue)
                        return Winner.One;
                    else if (playerOneHand.RankMadeUpCards[j].TrueValue < playerTwoHand.RankMadeUpCards[j].TrueValue)
                        return Winner.Two;
                }

                //compare highest value cards
                for (int i = 4; i >= 0; i--)
                {
                    if (playerOneHand.Cards[i].TrueValue > playerTwoHand.Cards[i].TrueValue)
                        return Winner.One;
                    else if (playerOneHand.Cards[i].TrueValue < playerTwoHand.Cards[i].TrueValue)
                        return Winner.Two;
                }
                return Winner.None;
            }



        }


    }
}
