﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    class Card
    {
        public Kind Kind { get; set; }
        public string Value { get; set; }

        public int TrueValue { get; set; }

        public Card(string card)
        {
            this.Kind = (Kind)Enum.Parse(typeof(Kind), card[1].ToString());
            this.Value = card[0].ToString();

            if (char.IsNumber(this.Value[0]))
                this.TrueValue = int.Parse(this.Value);
            else
                this.TrueValue = (int)((Value)Enum.Parse(typeof(Value), this.Value));

        }

        public override string ToString()
        {
            return this.Value + this.Kind.ToString()[0];
        }

    }
}
