﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    class CardValueComparer : IComparer<Card>
    {
        public int Compare(Card x, Card y)
        {
            if (x.TrueValue < y.TrueValue)
                return -1;
            else if (x.TrueValue == y.TrueValue)
                return 0;
            else
                return 1;
        }
    }
}
