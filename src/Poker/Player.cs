﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Poker
{
    class Player
    {
        public List<Card> Cards { get; private set; }
        public List<Card> RankMadeUpCards { get; set; }

        public Player(List<string> handStrings)
        {
            this.Cards = new List<Card>();
            this.RankMadeUpCards = new List<Card>();

            foreach (var item in handStrings)
            {
                Cards.Add(new Card(item));
            }

            this.Cards.Sort(new CardValueComparer());
        }

        public override string ToString()
        {
            return string.Join(" ", Cards);
        }

        public Rank CalculateRank()
        {
            if (this.Cards.All(x => x.TrueValue >= 10) && this.IsSameKind)
            {
                RankMadeUpCards.AddRange(this.Cards);
                return Rank.RoyalFlush;
            }
            else if (this.IsInConsecutiveOrder && this.IsSameKind)
            {
                RankMadeUpCards.AddRange(this.Cards);
                return Rank.StraightFlush;
            }
            else if (this.IsFourCardSameValue)
            {
                var fourSameCards = this.Cards.GroupBy(x => x.Value).Where(x => x.Count() == 4).First();
                this.RankMadeUpCards.AddRange(fourSameCards);
                return Rank.FourOfKind;
            }

            else if (this.IsFullHouse)
            {
                RankMadeUpCards.AddRange(this.Cards);
                return Rank.FullHouse;
            }

            else if (this.IsSameKind)
            {
                RankMadeUpCards.AddRange(this.Cards);
                return Rank.Flush;
            }
            else if (this.IsInConsecutiveOrder)
            {
                RankMadeUpCards.AddRange(this.Cards);
                return Rank.Straight;
            }

            else if (this.IsThreeCardSameValue)
            {
                var list = this.Cards.GroupBy(x => x.Value).First(x => x.Count() >= 3);
                this.RankMadeUpCards.AddRange(list);
                return Rank.ThreeOfAKind;
            }

            else if (this.HasTwoPairs)
            {
                var pairs = this.Cards.GroupBy(x => x.Value).Where(x => x.Count() >= 2).Select(x => x.ToList());
                foreach (var item in pairs)
                {
                    this.RankMadeUpCards.AddRange(item);
                }

                return Rank.TwoPairs;
            }
            else if (this.HasOnePair)
            {
                var pair = this.Cards.GroupBy(x => x.Value).FirstOrDefault(x => x.Count() >= 2);
                this.RankMadeUpCards.AddRange(pair);
                return Rank.Pair;
            }

            else
                return Rank.HighCard;



        }

        public bool IsSameKind
        {
            get
            {
                var kind = Cards.First().Kind;
                return this.Cards.All(x => x.Kind == kind);
            }

        }

        public bool IsInConsecutiveOrder
        {
            get
            {
                int nextValue = Cards.First().TrueValue;
                foreach (var card in Cards)
                {
                    if (nextValue == card.TrueValue)
                    {
                        nextValue++;
                        continue;
                    }
                    return false;

                }
                return true;
            }
        }

        public bool IsFourCardSameValue
        {
            get
            {
                int count = GetSameValueCardMaxCount();

                if (count == 4)
                    return true;
                return false;
            }
        }

        public bool IsThreeCardSameValue
        {
            get
            {
                int count = GetSameValueCardMaxCount();

                if (count == 3)
                    return true;
                return false;
            }
        }

        private int GetSameValueCardMaxCount()
        {
            var maxCount = this.Cards.GroupBy(x => x.Value).Max(x => x.Count());
            return maxCount;
        }

        public bool HasTwoPairs
        {
            get
            {
                var pairCount = this.Cards.GroupBy(x => x.Value).Count(x => x.Count() >= 2);
                return pairCount == 2;
            }
        }

        public bool HasOnePair
        {
            get
            {
                var pairCount = this.Cards.GroupBy(x => x.Value).Count(x => x.Count() >= 2);
                return pairCount == 1;
            }
        }

        public bool IsFullHouse
        {
            get
            {

                int count = 0;
                var testCard = this.Cards.First();
                List<Card> remainingCards = new List<Card>();
                List<Card> sameValueCards = new List<Card>();

                foreach (var card in this.Cards)
                {
                    if (card.Value == testCard.Value)
                    {
                        count++;
                        sameValueCards.Add(card);
                        continue;
                    }
                    else
                    {
                        count = 0;
                        sameValueCards.Clear();
                        remainingCards.Add(card);
                        testCard = card;
                    }
                }


                if (count < 2)
                    return false;

                //check three kind in remaining cards
                var sameKind = IsSameKindList(remainingCards);
                if (remainingCards.Count == 3)
                {
                    return true;
                }

                else
                {
                    //find matching one from sameValuecards
                    var testKindCard = remainingCards.First();
                    var matchingElement = sameValueCards.FirstOrDefault(x => x.Kind == testKindCard.Kind);
                    if (matchingElement != null)
                        return true;
                }
                return false;
            }

        }

        public bool IsSameKindList(List<Card> cards)
        {
            Kind testKind = cards.First().Kind;
            foreach (var card in cards)
            {
                if (card.Kind != testKind)
                    return false;
            }

            return true;
        }

    }
}
